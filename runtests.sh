#!/bin/bash
set -eu
IFS=$'\n\t'

CURRDIR="$(dirname "$0")"
CURRDIR="$(realpath "$CURRDIR")"
PAREDIR="$(realpath "$CURRDIR/..")"
cd "$CURRDIR"


# {{{ FUNCTIONS

# Function to copy the file at $1 to $2 if it exists
# @var $1 the file to check if it exists
# @var $2 the path where to copy it if we found it
copy_if_exists() {
	if [ "$RUN_IN_CI" == "1" ]
	then
		if [ -f "$1" ]
		then
			cp "$1" "$2"
		fi
	fi
}

# Function to check the availability of a given binary
# @var $1 the name of the binary to check
check_install() {
	local RES
	
	set +e
	command -v "$1" > /dev/null 2>&1
	RES=$?
	set -e
	if [ $RES != 0 ]
	then
		echo "[$(date '+%Y-%m-%d %H:%M:%S')] Failed to find $1, please install it"
		exit 1
	fi
}

# Function to download from $1 and store the result to $2
# @var $1 the url to download
# @var $2 the local path of the file to save
download() {
	check_install "curl"
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] DOWNLOADING : $1"
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] WRITING TO  : $2"
	curl --location --progress-bar --fail --show-error "$1" --output "$2"
}

# Function to get the full release url for the given library
# @var $1 the vendor name
# @var $2 the library name
get_release_url() {
	local API_URL
	local RELEASE_URL
	
	if [ "$2" == "phpunit" ]
	then
		echo "https://phar.phpunit.de/phpunit-9.phar"
	else
		check_install "curl"
		check_install "jq"
		check_install "sed"
		API_URL="https://api.github.com/repos/$1/$2/releases"
		RELEASE_URL=$(curl --location --progress-bar --fail --show-error "$API_URL" | jq '[.[]|.assets|.[]|.browser_download_url][0]' | sed 's/"//g')
		echo "$RELEASE_URL"
	fi
}

# Ensures that the given phar file is correctly installed
# @var $1 the path of the file where to search it on the file system if available
# @var $2 the path where the file should be executed
# @var $3 the package vendor name in the github url
# @var $4 the package library name in the github url
phar_install() {
	local OLDPSTN
	local RELEASE_URL
	
	check_install "find"
	copy_if_exists "$1" "$2"
	
	set +e
	OLDPSTN=$(find "$2" -mtime +7 -print > /dev/null 2>&1)
	set -e
	if [ ! -f "$2" ] || [ -n "$OLDPSTN" ]
	then
		RELEASE_URL=$(get_release_url "$3" "$4")
		download "$RELEASE_URL" "$2"
	else
		echo "[$(date '+%Y-%m-%d %H:%M:%S')] DO NOT INSTALL $3/$4 : not old enough"
	fi
}

# Tries to install the dependancies with composer
composer_install() {
	local COMPOSER_VERB
	local COMPOSER_ING
	local RET
	
	printf "\n"
	
	phar_install "/composer.phar" "$PAREDIR/composer.phar" "composer" "composer"
	
	if [ ! -f "$CURRDIR/vendor/autoload.php" ]
	then
		COMPOSER_VERB="install"
		COMPOSER_ING="INSTALLING"
	else
		COMPOSER_VERB="update"
		COMPOSER_ING="UPDATING"
	fi
	
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] $COMPOSER_ING composer dependancies"
	set +e
	php "$PAREDIR/composer.phar" "$COMPOSER_VERB" --ansi --no-interaction --no-progress --prefer-dist
	RET=$?
	set -e
	
	return $RET
}

# Tries to install composer dependancies multiple times
# @var $1 the number of retries
composer_install_retry() {
	local RET
	
	(composer_install)
	RET=$?
	
	if [ -z ${1+x} ]
	then
		"$1"=3
	fi
	
	LOOP=0
	while [[ $LOOP < $1 && "$RET" != "0" ]]
	do
		sleep "$((39 * (LOOP + 1)))s"
		LOOP=$((LOOP + 1))
		(composer_install)
		RET=$?
	done
}

# }}} END FUNCTIONS

check_install "awk"
check_install "cat"
check_install "curl"
check_install "grep"
check_install "head"
check_install "jq"
check_install "php"
check_install "sed"
printf "\n"

# @var PHPVERSION string the php version (we get "A.B")
# php -v :: PHP A.B.C (cli) (built: Aug XX YYYY HH:MM:SS) ( NTS )
PHPVERSION=$(php -v | grep PHP | grep cli | awk '{print $2}' | awk 'BEGIN{FS="."} {print $1"."$2}')
PHPVERSIONINT=$(($(php -v | grep PHP | grep cli | awk '{print $2}' | awk 'BEGIN{FS="."} {print $1$2}')))
echo "[$(date '+%Y-%m-%d %H:%M:%S')] RUNNING ON PHP : (string) $PHPVERSION / (int) $PHPVERSIONINT"

# @var RUN_IN_CI boolean whether this script runs in CI (gitlab...)
RUN_IN_CI=0

# argument management loop
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
for arg in "$@"
do
	case $arg in
		--ci*) RUN_IN_CI=1 ;;
		*)                 ;;
	esac
	shift # remove arg from "$@" and reorder $ positions
done


# {{{ BEGIN FORMAT composer.json
if [ $RUN_IN_CI == 0 ]
then
	
	printf "\n"
	# we dont need to rebuild composer.json hence dont need jq when in CI
	
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] REORDERING : $CURRDIR/composer.json"
	CURRCOMP=$(cat "$CURRDIR/composer.json")
	# reorder composer.json according to schema
	# https://getcomposer.org/doc/04-schema.md
	# except minimum stability
	# pretty print with tabs
	# then ignore null fields
	# then add space before colon
	echo "$CURRCOMP" | jq --tab '{ 
		name: .name,
		description: .description,
		version: .version,
		type: .type,
		keywords: .keywords,
		homepage: .homepage,
		readme: .readme,
		time: .time,
		license: .license,
		authors: .authors,
		support: .support,
		funding: .funding,
		require: .require,
		"require-dev": ."require-dev",
		conflict: .conflict,
		replace: .replace,
		provide: .provide,
		suggest: .suggest,
		autoload: .autoload,
		"autoload-dev": ."autoload-dev",
		"include-path": ."include-path",
		"target-dir": ."target-dir",
		"prefer-stable": ."prefer-stable",
		repositories: .repositories,
		config: .config,
		scripts: .scripts,
		extra: .extra,
		bin: .bin,
		archive: .archive,
		abandoned: .abandoned,
		"non-feature-branches": ."non-feature-branches"
	} | del(.[] | nulls)' | sed 's/":/" :/g' | head -c -1 > "$CURRDIR/composer.json"
fi
# }}} END FORMAT composer.json


# {{{ BEGIN RUN COMPOSER

composer_install_retry 3

# }}} END RUN COMPOSER
